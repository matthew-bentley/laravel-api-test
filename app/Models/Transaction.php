<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Transaction class
 *
 * @package App\Models
 *
 * Database Columns
 * @property int $id
 * @property int $orderId
 * @property string $buyerName
 * @property float $salePrice
 *
 * Relationships
 * @property Order $order
 */
class Transaction extends Model
{
    /** @var string */
    protected $table = 'Transaction';

    /** @var string */
    protected $primaryKey = 'id';

    /**
     * How a Transaction should be formatted when converted to an array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id,
            'buyerName' => $this->buyerName,
            'salePrice' => number_format($this->salePrice, 2),
        ];
    }

    /**
     * Get the parent Order that this Transaction belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'orderId', 'id');
    }
}
