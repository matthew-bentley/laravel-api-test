<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Order class
 *
 * @package App\Models
 *
 * Database Columns
 * @property int $id
 * @property string $name
 * @property string $imageUrl
 * @property string $status
 * @property float $price
 *
 * Relationships
 * @property Transaction $transaction
 */
class Order extends Model
{
    // Valid Order Statuses
    const STATUS_OPEN = 'open';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_SOLD = 'sold';

    /** @var string */
    protected $table = 'Order';

    /** @var string */
    protected $primaryKey = 'id';

    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = [
        'name',
        'imageUrl',
        'status',
        'price',
    ];

    /**
     * Validation rules for an Order
     *
     * @return array
     */
    public static function validationRules() : array
    {
        return [
            'name' => 'required|max:255',
            'imageUrl' => 'required|max:255',
            'price' => 'required|numeric',
            'status' => 'require|in:open,accept,sold'
        ];
    }

    /**
     * How an Order should be formatted when converted to an array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'imageUrl' => $this->imageUrl,
            'status' => ucfirst($this->status),
            'price' => number_format($this->price, 2),
            'transaction' => empty($this->transaction) ? new \stdClass() : $this->transaction,
        ];
    }

    /**
     * Get the transaction (if any) that is attached to this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaction()
    {
        // Can easily be changed to a One-to-Many relationship by using `hasMany` in place of `hasOne`
        return $this->hasOne('App\Models\Transaction', 'orderId', 'id');
    }
}
