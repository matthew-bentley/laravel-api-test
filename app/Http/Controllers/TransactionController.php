<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * TransactionController class
 *
 * This class contains all actions that are directly related to Transactions
 *
 * @package App\Http\Controllers
 */
class TransactionController extends BaseController
{
    /**
     * Get Orders with Transactions placed by the buyer with the provided name
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function withBuyerName(Request $request)
    {
        $buyerName = $request->input('buyerName');

        // @TODO Paginate Results

        // Get Orders with Transactions placed by the buyer with the provided name
        $orders = Order::whereHas('transaction', function (Builder $query) use ($buyerName) {
            $query->where('buyerName', $buyerName);
        })->get();

        return response()->json($orders);
    }

    /**
     * Deleted the transaction with the provided ID
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function deleteTransaction(Request $request)
    {
        $transactionId = intval($request->input('id'));

        // Ensure that transaction exists
        $transaction = Transaction::with('order')->find($transactionId);
        if (empty($transaction)) {
            // Return Error
            return response()->json(['error' => "Order with ID: {$transactionId} does not exist."], 404);
        }

        // Delete the Transaction with the provided ID
        if (!$transaction->delete()) {
            return response()->json(['error' => "There was an error removing Transaction #{$transactionId}"], 500);
        }

        // Set status of Order to Accepted
        $transaction->order->status = Order::STATUS_ACCEPTED;

        if ($transaction->order->save()) {
            // Return the Order connected to this transaction
            return response()->json($transaction->order);
        } else {
            return response()->json(['error' => "There was an error updating Order #{$transaction->orderId}"], 500);
        }
    }
}
