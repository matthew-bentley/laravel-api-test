<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * OrderController class
 *
 * This class contains all actions that are directly related to Orders
 *
 * @package App\Http\Controllers
 */
class OrderController extends BaseController
{
    /**
     * Create an order
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createSale(Request $request)
    {
        // Capture and validate input before saving
        $validationRules = Order::validationRules();

        $validatedData = $this->validate($request, [
            'name' => $validationRules['name'],
            'imageUrl' => $validationRules['imageUrl'],
            'price' => $validationRules['price'],
        ]);

        // Create Order
        $order = new Order();
        $order->name = $validatedData['name'];
        $order->imageUrl = $validatedData['imageUrl'];
        $order->price = floatval($validatedData['price']);

        if ($order->save()) {
            // Get any default values that may have been automatically be set
            $order->refresh();
            // Return newly created order
            return response()->json($order, 201);
        } else {
            return response()->json(['error' => 'There was an error creating an Order.'], 500);
        }
    }

    /**
     * Get orders with the provided status
     *
     * @param string $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSales(string $status)
    {
        // @TODO Paginate Results

        return response()
            ->json(Order::with('transaction')
                ->where('status', $status)
                ->get()
            );
    }

    /**
     * Update the status of the order with the provided
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request)
    {
        // Capture Input
        $orderId = intval($request->input('id'));

        // Ensure that the order exists
        $order = Order::find($orderId);
        if (empty($order)) {
            // Return Error
            return response()->json(['error' => "Order with ID: {$orderId} does not exist."], 404);
        }

        // Handle 'action' parameter
        $action = intval($request->input('action'));
        switch ($action) {
            case 'accept' :
                $order->status = Order::STATUS_ACCEPTED;
                break;
            // @TODO Add more conditions to update the status to more than just the "accepted" state
        }

        if ($order->save()) {
            return response()->json($order);
        } else {
            return response()->json(['error' => "There was an error updating Order #{$orderId}"], 500);
        }
    }

    /**
     * Update Image URL
     *
     * This action will update the Order's Image URL with the provided ID
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateImageUrl(Request $request)
    {
        // Capture Input
        $orderId = intval($request->input('id'));

        // Ensure that the order exists
        $order = Order::find($orderId);
        if (empty($order)) {
            // Return Error
            return response()->json(['error' => "Order with ID: {$orderId} does not exist."], 404);
        }

        $validationRules = Order::validationRules();

        $validatedData = $this->validate($request, [
            'imageUrl' => $validationRules['imageUrl'],
        ]);

        $order->imageUrl = $validatedData['imageUrl'];

        if ($order->save()) {
            // Return updated order
            return response()->json($order);
        } else {
            return response()->json(['error' => 'There was an error creating an Order.'], 500);
        }
    }
}
