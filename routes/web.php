<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('sale', 'OrderController@createSale');
$router->get('sales/{status:open|accepted|sold}', 'OrderController@getSales');
$router->patch('sale/status', 'OrderController@updateStatus');
$router->patch('sale/image-url', 'OrderController@updateImageUrl');
$router->get('transaction/with-buyer-name', 'TransactionController@withBuyerName');
$router->delete('transaction', 'TransactionController@deleteTransaction');
