# Snapwire Coding Challenge

## Local Dev Environment Setup

### Prerequisites
- Install Docker Desktop
- Ensure nothing is currently using port 80, 9000, or 3306 on localhost before starting up this environment

### Installation
- Clone this Git Repo
- Open a terminal
- `cd {project folder}` Move into the root project folder where you cloned the git repo
- `cp .env.example .env` Copy the example environment variable file to the one that will actually be used.  No changes 
need to be made to this file out of the box
- `composer install` Install all composer dependencies
    - You may need to run `composer install --ignore-platform-reqs` if you don't have the latest version of PHP on your 
    host machine
- `docker-compose up -d` Start up the Docker containers
- `docker-compose exec app php /app/artisan migrate` Run all database migrations
- The base URL is `localhost` for all API endpoints
- To stop this environment, run `docker-compose stop`

## Database
Database Name: `RestAPI`

You can connect to the MySQL Console using these commands:
- `docker-compose exec db bash` - This will connect you to the bash shell of the MySQL Docker container
- `mysql -u root -p` (then use "root" as the password), now you'll be in the MySQL console client
- Use `exit` to leave the MySQL Console Client and `exit` to leave the base shell of the MySQL DOcker Container

You can also connect your favorite MySQL GUI tool using the following information:
Host: `12.0.0.1`
Username: `root`
Password: `root`
Database: `RestAPI`
Port: `3306`

## Data Models

### Order
Database Table: `Order`

Columns:
- `id` Integer Primary Key
- `name` String Name of the seller
- `imageUrl` String URL of the image 
- `status` String Status of the sale, valid values: "open", "accepted", or "sold"
- `price` Double Price of the photo

Indexes:
- `status` Indexed because there are a few endpoints fetching Orders based on their status. 

Relationships:
- Transaction: Currently there is a One-to-One relationship between and Order and a Transaction.
 This could easily be made into a One-to-Many relationship as the Transaction has an `orderId` column on which this 
 relationship could be based.  

### Transaction
Database Table: `Order`

Columns:
- `id` Integer Primary Key
- `orderId` Integer The ID of this Transaction's parent order
- `buyerName` String Name of the buyer
- `salePrice` Double Price of the sale

Indexes:
- `id` Primary Key
- `orderId` Used in the Order-Transaction relationship
- `buyerName` Indexed because there of query involving the Buyer's name

Relationships:
- Order: A One-to-One relationship between and Transaction and it's parent Order. 

## API Endpoints

### Create a Sale
URL: `POST localhost/sale`
Expected Body :
```json
{
    "name": STRING,
    "imageUrl": STRING,
    "price": NUMBER
}
```

### Get Open Sales
URL `GET localhost/sales/open`

Next Steps: Paginate output

### Get Accepted Sales
URL `GET localhost/sales/accepted`

Next Steps: Paginate output

### Get Sold Sales
URL `GET localhost/sales/sold`

Next Steps: Paginate output

### Get Specific Buyer Purchases
URL: `GET localhost/transaction/with-buyer-name`
Expected Body :
```json
{
    "buyerName": STRING,
}
```
Next Steps: Paginate output

### Delete a Transaction
URL: `DELETE localhost/transaction`
Expected Body :
```json
{
    "id": INTEGER,
}
```

### Accept a Sale
URL: `PATCH sale/status`
Expected Body :
```json
{
    "id": INTEGER,
    "action": "accept"
}
```

Next Steps: Add ability to handle different statues

### Change the Image URL of an Image
URL: `PATCH sale/image-url`
Expected Body :
```json
{
    "id": INTEGER,
    "imageUrl": STRING
}
```
